var lab3__FrontUI_8py =
[
    [ "on_keypress", "lab3__FrontUI_8py.html#a215d0d9cf2ac79d94ac50b780d1b6a91", null ],
    [ "readSerial", "lab3__FrontUI_8py.html#a7090ff9de1b074faad6c8a9c76be29b5", null ],
    [ "sendChar", "lab3__FrontUI_8py.html#af79b343ddba725436957c5907493069c", null ],
    [ "ax", "lab3__FrontUI_8py.html#ab33641258e548b0823dc081b8b0c0ab7", null ],
    [ "charSent", "lab3__FrontUI_8py.html#ab29669eebe02b067d7604bc43e9d3eff", null ],
    [ "csvlist", "lab3__FrontUI_8py.html#ab676a919c832fbda2e93025574da7401", null ],
    [ "csvwriter", "lab3__FrontUI_8py.html#ad3aa2c58d547ba0a0bbc8dd78b23d27b", null ],
    [ "data", "lab3__FrontUI_8py.html#aba49c13905eb53a245a14677685e1d40", null ],
    [ "dataStop", "lab3__FrontUI_8py.html#ae778f5f2b611834297d2043c5145cd4e", null ],
    [ "endScript", "lab3__FrontUI_8py.html#aded722b895cc10b8d7a1145642e2a098", null ],
    [ "fig", "lab3__FrontUI_8py.html#a3202ae12d7039088ecd2e578a335ae44", null ],
    [ "n", "lab3__FrontUI_8py.html#a6a6e656e61fe1692ef9d0447fddcfc5f", null ],
    [ "p", "lab3__FrontUI_8py.html#a3b725392a64a815dd914bfd64c5f8ad3", null ],
    [ "phtime", "lab3__FrontUI_8py.html#a8d5d2982ef107b70103b228ccdc43d34", null ],
    [ "phvol", "lab3__FrontUI_8py.html#a0bd4858eeb85738c626cc8cf86c83d0f", null ],
    [ "pushed_key", "lab3__FrontUI_8py.html#a5811c548bd8a8b14690dcfae25ecbeaf", null ],
    [ "ser", "lab3__FrontUI_8py.html#a3489784502db8e517d869d99249e6929", null ],
    [ "time", "lab3__FrontUI_8py.html#a59c9318bc9e6b32468e7a5fb212a4f88", null ],
    [ "voltage", "lab3__FrontUI_8py.html#a7ef12959d93a9990bd7a787f9c178278", null ]
];