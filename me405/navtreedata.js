/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Project Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0x01: Reintroduction to Python and Finite State Machines", "lab01.html", [
      [ "Problem Statement", "lab01.html#sec_problem1", null ],
      [ "Code Documentation", "lab01.html#documentation1", null ],
      [ "Source Code", "lab01.html#sourcecode1", null ],
      [ "State Transition Diagram", "lab01.html#state_transition_1", null ]
    ] ],
    [ "Lab 0x02: Think Fast", "lab02.html", [
      [ "Problem Statement", "lab02.html#sec_problem2", null ],
      [ "Code Documentation", "lab02.html#documentation2", null ],
      [ "Source Code", "lab02.html#sourcecode2", null ]
    ] ],
    [ "Lab 0x03: Pushing the Right Buttons", "Lab03.html", [
      [ "Problem Statement", "Lab03.html#sec_problem3", null ],
      [ "Code Documentation", "Lab03.html#documentation3", null ],
      [ "Source Code and Example Response", "Lab03.html#sourcecode3", null ],
      [ "Example Step Response Plot", "Lab03.html#exampleresponse3", null ],
      [ "State Transition Diagram", "Lab03.html#state_transition_3", null ]
    ] ],
    [ "Lab 0x04: Hot or Not?", "Lab04.html", [
      [ "Problem Statement", "Lab04.html#sec_problem4", null ],
      [ "Code Documentation", "Lab04.html#documentation4", null ],
      [ "Source Code and Example Response", "Lab04.html#sourcecode4", null ],
      [ "Example Temperature Data", "Lab04.html#exampleresponse4", null ]
    ] ],
    [ "Lab 0x05: Feeling Tipsy?", "Lab05.html", [
      [ "Problem Statement", "Lab05.html#sec_problem5", null ],
      [ "Calculations", "Lab05.html#sec_calculations5", null ]
    ] ],
    [ "Lab 0x06: Simulation or Reality?", "Lab06.html", [
      [ "Problem Statement", "Lab06.html#sec_problem6", null ],
      [ "Source Code", "Lab06.html#sourcecode6", null ],
      [ "Simulink Block Diagrams", "Lab06.html#blockdiagram6", null ],
      [ "Open-Loop Simulation Results", "Lab06.html#simresultsOL6", null ],
      [ "Closed-Loop Simulation Results", "Lab06.html#simresultsCL6", null ],
      [ "Jacobian Linearization Calculations", "Lab06.html#jacobianlinearization", null ]
    ] ],
    [ "Lab 0x07: Feeling Touchy", "Lab0x07.html", [
      [ "Problem Statement", "Lab0x07.html#sec_problem7", null ],
      [ "Code Documentation", "Lab0x07.html#documentation7", null ],
      [ "Source Code", "Lab0x07.html#sourcecode7", null ]
    ] ],
    [ "Lab 0x08: Term Project Part I", "Lab0x08.html", [
      [ "Problem Statement", "Lab0x08.html#sec_problem8", null ],
      [ "Code Documentation", "Lab0x08.html#documentation8", null ],
      [ "Source Code", "Lab0x08.html#sourcecode8", null ]
    ] ],
    [ "Lab 0x09: Term Project Part II", "Lab0x09.html", [
      [ "Problem Statement", "Lab0x09.html#sec_problem9", null ],
      [ "Controller Gains", "Lab0x09.html#controllergains", null ],
      [ "Code Documentation", "Lab0x09.html#documentation9", null ],
      [ "Source Code", "Lab0x09.html#sourcecode9", null ],
      [ "Controller in Action", "Lab0x09.html#platformvideo", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classlab7__main_1_1TouchDriver.html#ae06bb7536efb5e93cbca16b16fdc3e9a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';