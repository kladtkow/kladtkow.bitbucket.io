var searchData=
[
  ['x_360',['X',['../TermProject_8py.html#a42814847a11876df2753b038b7e1bf6f',1,'TermProject']]],
  ['x_5fm_361',['x_m',['../classlab7__main_1_1TouchDriver.html#a524d1144db64912a87723db49e702079',1,'lab7_main::TouchDriver']]],
  ['x_5fp_362',['x_p',['../classlab7__main_1_1TouchDriver.html#a318f09c1e3cb84e4d0dba781c469cf97',1,'lab7_main::TouchDriver']]],
  ['xcoefficient_363',['xcoefficient',['../classlab7__main_1_1TouchDriver.html#a93d447f745a0ec82f12de8c4ea3828ec',1,'lab7_main::TouchDriver']]],
  ['xcoord_364',['xcoord',['../classlab7__main_1_1TouchDriver.html#a1e8a8adbcf156549edd4a3d4cbbfb6ef',1,'lab7_main::TouchDriver']]],
  ['xdot_365',['Xdot',['../TermProject_8py.html#a5625798c2ea602317d05091c19b870b4',1,'TermProject']]],
  ['xintercept_366',['xintercept',['../classlab7__main_1_1TouchDriver.html#a458ebaf46a399a130f70173432e7f9c9',1,'lab7_main::TouchDriver']]],
  ['xtick_367',['xtick',['../classlab7__main_1_1TouchDriver.html#a7e1f6f16fe8ae27773d3cc673251401e',1,'lab7_main::TouchDriver']]],
  ['xtickhigh_368',['xtickhigh',['../classlab7__main_1_1TouchDriver.html#a00ca6b28af211cf2e3f2ed7cf8d49a8f',1,'lab7_main::TouchDriver']]],
  ['xticklow_369',['xticklow',['../classlab7__main_1_1TouchDriver.html#a921625373a4bb15a9f0a58d7af1c0b67',1,'lab7_main::TouchDriver']]]
];
