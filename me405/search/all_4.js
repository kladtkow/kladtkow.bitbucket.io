var searchData=
[
  ['data_29',['data',['../classmcp9808_1_1mcp9808.html#a205349834cff4c4f8eff670205c7a0f9',1,'mcp9808.mcp9808.data()'],['../lab3__FrontUI_8py.html#aba49c13905eb53a245a14677685e1d40',1,'lab3_FrontUI.data()']]],
  ['datastop_30',['dataStop',['../lab3__FrontUI_8py.html#ae778f5f2b611834297d2043c5145cd4e',1,'lab3_FrontUI']]],
  ['delta_31',['Delta',['../classLab0x09EncoderDriver_1_1EncoderDriver.html#a63627800005aad44884719c71b1f9c02',1,'Lab0x09EncoderDriver.EncoderDriver.Delta()'],['../classLab__0x08__EncoderDriver_1_1EncoderDriver.html#a44786e9fa02b5f6c17a7e00638318f79',1,'Lab_0x08_EncoderDriver.EncoderDriver.Delta()']]],
  ['deltax_32',['deltax',['../classlab7__main_1_1TouchDriver.html#a1f0381505e30af42256c04aa570c3f91',1,'lab7_main::TouchDriver']]],
  ['deltay_33',['deltay',['../classlab7__main_1_1TouchDriver.html#a7bbff5d1edb0dd8b285e0ce32d766667',1,'lab7_main::TouchDriver']]],
  ['disable_34',['disable',['../classLab0x09MotorDriver_1_1MotorDriver.html#a43af44a326ec4d3ccf88fdc78ee7657f',1,'Lab0x09MotorDriver.MotorDriver.disable()'],['../classLab__0x08__MotorDriver_1_1MotorDriver.html#ae16ac3bede928d2460fa6fad4607809a',1,'Lab_0x08_MotorDriver.MotorDriver.disable()']]]
];
