var annotated_dup =
[
    [ "Lab0x09EncoderDriver", null, [
      [ "EncoderDriver", "classLab0x09EncoderDriver_1_1EncoderDriver.html", "classLab0x09EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab0x09MotorDriver", null, [
      [ "MotorDriver", "classLab0x09MotorDriver_1_1MotorDriver.html", "classLab0x09MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab0x09TouchScreen", null, [
      [ "TouchScreen", "classLab0x09TouchScreen_1_1TouchScreen.html", "classLab0x09TouchScreen_1_1TouchScreen" ]
    ] ],
    [ "lab2_main", null, [
      [ "LEDDriver", "classlab2__main_1_1LEDDriver.html", "classlab2__main_1_1LEDDriver" ]
    ] ],
    [ "lab7_main", null, [
      [ "TouchDriver", "classlab7__main_1_1TouchDriver.html", "classlab7__main_1_1TouchDriver" ]
    ] ],
    [ "Lab_0x08_EncoderDriver", null, [
      [ "EncoderDriver", "classLab__0x08__EncoderDriver_1_1EncoderDriver.html", "classLab__0x08__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab_0x08_MotorDriver", null, [
      [ "MotorDriver", "classLab__0x08__MotorDriver_1_1MotorDriver.html", "classLab__0x08__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ]
];