var lab6__FrontUI_8py =
[
    [ "readSerial", "lab6__FrontUI_8py.html#ace4b812e99a2656caf3de606eff573e9", null ],
    [ "sendChar", "lab6__FrontUI_8py.html#a57ec1d20329aeed61e40ad74f327b4fa", null ],
    [ "ax", "lab6__FrontUI_8py.html#a020534f2600eef61022893b2711bbe90", null ],
    [ "data", "lab6__FrontUI_8py.html#a6bfa7438d98c63921263eb173c929ada", null ],
    [ "dataStop", "lab6__FrontUI_8py.html#a5991028b537015f7e957df76ec103621", null ],
    [ "endScript", "lab6__FrontUI_8py.html#ae1a411e7f6ffa6bd75b0813ec4e2988c", null ],
    [ "fig", "lab6__FrontUI_8py.html#a0489dfccbeb1fa32d02761dcee1effb4", null ],
    [ "inv", "lab6__FrontUI_8py.html#af3f47abee9d34b84ce9b57f4119fb39c", null ],
    [ "n", "lab6__FrontUI_8py.html#a78c6ab249b0704c0355a5e15cb212103", null ],
    [ "p", "lab6__FrontUI_8py.html#a0a837b59aee09a3f1a65919dcbda15c6", null ],
    [ "phspeed", "lab6__FrontUI_8py.html#ac6c3c2111f6524fb5dc54b6260bb3e9d", null ],
    [ "phtime", "lab6__FrontUI_8py.html#af66feebf6e62a446b72aeca2c1e0f454", null ],
    [ "sent_cmd", "lab6__FrontUI_8py.html#a206d69662e73e98be5ef21a039a0c459", null ],
    [ "ser", "lab6__FrontUI_8py.html#a8d389e61f786e37c0e2db69e6592facb", null ],
    [ "speed", "lab6__FrontUI_8py.html#adce2a0bc4ebe0ddccb3953724117498f", null ],
    [ "time", "lab6__FrontUI_8py.html#aa513bcf39ff63698ea433931a93d72b3", null ]
];