var classlab6__NucleoUI_1_1TaskNucleoUI =
[
    [ "__init__", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a3d32c1828dc2da590fc146567ab3b6eb", null ],
    [ "printDebug", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a244c9fc8a5d5e68ab8dfa1b348afd59b", null ],
    [ "run", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a873391c491c4edafa8d6aea0c140dd2a", null ],
    [ "transitionTo", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a6d4fbd0ef465f6a90c609554d2c5d178", null ],
    [ "curr_time", "classlab6__NucleoUI_1_1TaskNucleoUI.html#ad4c516da292fc44646bff0a797757401", null ],
    [ "dbg", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a1c048543a60e54b58fcb65b3d73f5984", null ],
    [ "interval", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a89da1ccb6206be156b1832ffda5354b9", null ],
    [ "next_time", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a24d93998493b20b2db3357237720a4d4", null ],
    [ "runs", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a4e5496a21da21776529ad11dece18f10", null ],
    [ "ser", "classlab6__NucleoUI_1_1TaskNucleoUI.html#add082801369063990b7e0758152664b2", null ],
    [ "start_time", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a076002df2684e72e65e139b27c44cb34", null ],
    [ "state", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a0baeda0c0b8f0d94dc421f68358f8071", null ],
    [ "taskNum", "classlab6__NucleoUI_1_1TaskNucleoUI.html#a522b6d9e885abfb3028fc67a8431edfc", null ]
];