var classlab6__controller_1_1MotorDriver =
[
    [ "__init__", "classlab6__controller_1_1MotorDriver.html#aea3eb5e9535bdbb7745046eae878d8c8", null ],
    [ "disable", "classlab6__controller_1_1MotorDriver.html#a3fabe9bc78083b6245d99080ad76efd1", null ],
    [ "enable", "classlab6__controller_1_1MotorDriver.html#ae1d182481c8add41fb04ca319065c2cf", null ],
    [ "set_duty", "classlab6__controller_1_1MotorDriver.html#aad7295a43cdd788ec354fdd3f09cd009", null ],
    [ "IN1", "classlab6__controller_1_1MotorDriver.html#aea93616c9d926b890c071818172196e4", null ],
    [ "IN2", "classlab6__controller_1_1MotorDriver.html#a50a3d3e0da9b9c735ed7d2994747cad9", null ],
    [ "motor_number", "classlab6__controller_1_1MotorDriver.html#a9e52ac17c9c505c3bd4c024ad45ce3ad", null ],
    [ "motor_state", "classlab6__controller_1_1MotorDriver.html#a41fca90a72e9b0387cecfa30ba8c16db", null ],
    [ "nSleep", "classlab6__controller_1_1MotorDriver.html#a2b4f9dc9b878321e18ffa53d5785e1ed", null ],
    [ "timer", "classlab6__controller_1_1MotorDriver.html#ac0a3cbc61fa099783d530c9e24c23909", null ]
];