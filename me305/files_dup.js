var files_dup =
[
    [ "HW0x00_FSM_ME305.py", "HW0x00__FSM__ME305_8py.html", [
      [ "TaskElevator", "classHW0x00__FSM__ME305_1_1TaskElevator.html", "classHW0x00__FSM__ME305_1_1TaskElevator" ],
      [ "Button", "classHW0x00__FSM__ME305_1_1Button.html", "classHW0x00__FSM__ME305_1_1Button" ],
      [ "elevatorInstance", "classHW0x00__FSM__ME305_1_1elevatorInstance.html", "classHW0x00__FSM__ME305_1_1elevatorInstance" ],
      [ "ElevatorPosition", "classHW0x00__FSM__ME305_1_1ElevatorPosition.html", "classHW0x00__FSM__ME305_1_1ElevatorPosition" ],
      [ "MotorDriver", "classHW0x00__FSM__ME305_1_1MotorDriver.html", "classHW0x00__FSM__ME305_1_1MotorDriver" ]
    ] ],
    [ "HW0x00_MAIN_ME305.py", "HW0x00__MAIN__ME305_8py.html", "HW0x00__MAIN__ME305_8py" ],
    [ "lab01_main.py", "lab01__main_8py.html", "lab01__main_8py" ],
    [ "lab2b_FSM.py", "lab2b__FSM_8py.html", [
      [ "taskLEDPattern", "classlab2b__FSM_1_1taskLEDPattern.html", "classlab2b__FSM_1_1taskLEDPattern" ],
      [ "VirtualLED", "classlab2b__FSM_1_1VirtualLED.html", "classlab2b__FSM_1_1VirtualLED" ],
      [ "mcuLED", "classlab2b__FSM_1_1mcuLED.html", "classlab2b__FSM_1_1mcuLED" ],
      [ "taskSawDimmer", "classlab2b__FSM_1_1taskSawDimmer.html", "classlab2b__FSM_1_1taskSawDimmer" ]
    ] ],
    [ "lab2b_MAIN.py", "lab2b__MAIN_8py.html", "lab2b__MAIN_8py" ],
    [ "lab3_comms.py", "lab3__comms_8py.html", "lab3__comms_8py" ],
    [ "lab3_encoder.py", "lab3__encoder_8py.html", [
      [ "TaskEncoder", "classlab3__encoder_1_1TaskEncoder.html", "classlab3__encoder_1_1TaskEncoder" ],
      [ "EncoderDriver", "classlab3__encoder_1_1EncoderDriver.html", "classlab3__encoder_1_1EncoderDriver" ]
    ] ],
    [ "lab3_interface.py", "lab3__interface_8py.html", [
      [ "TaskUI", "classlab3__interface_1_1TaskUI.html", "classlab3__interface_1_1TaskUI" ]
    ] ],
    [ "lab3_main.py", "lab3__main_8py.html", "lab3__main_8py" ],
    [ "lab4_comms.py", "lab4__comms_8py.html", "lab4__comms_8py" ],
    [ "lab4_DataGen.py", "lab4__DataGen_8py.html", [
      [ "TaskDataGen", "classlab4__DataGen_1_1TaskDataGen.html", "classlab4__DataGen_1_1TaskDataGen" ],
      [ "EncoderDriver", "classlab4__DataGen_1_1EncoderDriver.html", "classlab4__DataGen_1_1EncoderDriver" ]
    ] ],
    [ "lab4_DataUI.py", "lab4__DataUI_8py.html", [
      [ "TaskDataUI", "classlab4__DataUI_1_1TaskDataUI.html", "classlab4__DataUI_1_1TaskDataUI" ]
    ] ],
    [ "lab4_FrontUI.py", "lab4__FrontUI_8py.html", "lab4__FrontUI_8py" ],
    [ "lab5_comms.py", "lab5__comms_8py.html", "lab5__comms_8py" ],
    [ "lab5_interface.py", "lab5__interface_8py.html", [
      [ "TaskUI", "classlab5__interface_1_1TaskUI.html", "classlab5__interface_1_1TaskUI" ]
    ] ],
    [ "lab5_LED.py", "lab5__LED_8py.html", [
      [ "TaskBlinker", "classlab5__LED_1_1TaskBlinker.html", "classlab5__LED_1_1TaskBlinker" ],
      [ "LEDDriver", "classlab5__LED_1_1LEDDriver.html", "classlab5__LED_1_1LEDDriver" ]
    ] ],
    [ "lab5_main.py", "lab5__main_8py.html", "lab5__main_8py" ],
    [ "lab6_comms.py", "lab6__comms_8py.html", "lab6__comms_8py" ],
    [ "lab6_controller.py", "lab6__controller_8py.html", [
      [ "TaskMotorController", "classlab6__controller_1_1TaskMotorController.html", "classlab6__controller_1_1TaskMotorController" ],
      [ "ClosedLoop", "classlab6__controller_1_1ClosedLoop.html", "classlab6__controller_1_1ClosedLoop" ],
      [ "EncoderDriver", "classlab6__controller_1_1EncoderDriver.html", "classlab6__controller_1_1EncoderDriver" ],
      [ "MotorDriver", "classlab6__controller_1_1MotorDriver.html", "classlab6__controller_1_1MotorDriver" ]
    ] ],
    [ "lab6_FrontUI.py", "lab6__FrontUI_8py.html", "lab6__FrontUI_8py" ],
    [ "lab6_NucleoUI.py", "lab6__NucleoUI_8py.html", [
      [ "TaskNucleoUI", "classlab6__NucleoUI_1_1TaskNucleoUI.html", "classlab6__NucleoUI_1_1TaskNucleoUI" ]
    ] ],
    [ "lab7_comms.py", "lab7__comms_8py.html", "lab7__comms_8py" ],
    [ "lab7_controller.py", "lab7__controller_8py.html", [
      [ "TaskMotorController", "classlab7__controller_1_1TaskMotorController.html", "classlab7__controller_1_1TaskMotorController" ],
      [ "ClosedLoop", "classlab7__controller_1_1ClosedLoop.html", "classlab7__controller_1_1ClosedLoop" ],
      [ "EncoderDriver", "classlab7__controller_1_1EncoderDriver.html", "classlab7__controller_1_1EncoderDriver" ],
      [ "MotorDriver", "classlab7__controller_1_1MotorDriver.html", "classlab7__controller_1_1MotorDriver" ]
    ] ],
    [ "lab7_FrontUI.py", "lab7__FrontUI_8py.html", "lab7__FrontUI_8py" ],
    [ "lab7_NucleoUI.py", "lab7__NucleoUI_8py.html", [
      [ "TaskNucleoUI", "classlab7__NucleoUI_1_1TaskNucleoUI.html", "classlab7__NucleoUI_1_1TaskNucleoUI" ]
    ] ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "main6.py", "main6_8py.html", "main6_8py" ],
    [ "main7.py", "main7_8py.html", "main7_8py" ]
];