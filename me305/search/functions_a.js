var searchData=
[
  ['sendchar_207',['sendChar',['../lab4__FrontUI_8py.html#aa7c59dbd9132ee8e1c11a7464ae99cf4',1,'lab4_FrontUI.sendChar()'],['../lab6__FrontUI_8py.html#a57ec1d20329aeed61e40ad74f327b4fa',1,'lab6_FrontUI.sendChar()'],['../lab7__FrontUI_8py.html#a91271bf5552465af9e45a570b5a709e2',1,'lab7_FrontUI.sendChar()']]],
  ['set_5fduty_208',['set_duty',['../classlab6__controller_1_1MotorDriver.html#aad7295a43cdd788ec354fdd3f09cd009',1,'lab6_controller.MotorDriver.set_duty()'],['../classlab7__controller_1_1MotorDriver.html#ad17814fedfdcf5e3040e2467e4981cf0',1,'lab7_controller.MotorDriver.set_duty()']]],
  ['set_5fkp_209',['set_Kp',['../classlab6__controller_1_1ClosedLoop.html#ae15b88d63ebf25f9314bd9e480cc5c7c',1,'lab6_controller.ClosedLoop.set_Kp()'],['../classlab7__controller_1_1ClosedLoop.html#a38d6e87ecd69074006f9ca1627b90048',1,'lab7_controller.ClosedLoop.set_Kp()']]],
  ['set_5fomega_210',['set_Omega',['../classlab7__controller_1_1ClosedLoop.html#a13812f72a0c53899382783f07aa0c562',1,'lab7_controller::ClosedLoop']]],
  ['stop_211',['Stop',['../classHW0x00__FSM__ME305_1_1MotorDriver.html#acc994b794389895955b725b88e3704e7',1,'HW0x00_FSM_ME305::MotorDriver']]]
];
