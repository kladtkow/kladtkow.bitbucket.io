var lab7__FrontUI_8py =
[
    [ "readSerial", "lab7__FrontUI_8py.html#ae60df2e7f9c02954d30ea5192e0fc2c2", null ],
    [ "sendChar", "lab7__FrontUI_8py.html#a91271bf5552465af9e45a570b5a709e2", null ],
    [ "angle_diff", "lab7__FrontUI_8py.html#af9ae2eebbc1f92e88068f0b77dccb5ea", null ],
    [ "data", "lab7__FrontUI_8py.html#a45bdbb99299579cb5923dd5e3067f804", null ],
    [ "dataStop", "lab7__FrontUI_8py.html#a62d358edc96b262c7ef6a465c0786fb8", null ],
    [ "endScript", "lab7__FrontUI_8py.html#a6a812b057ba3ffd748edcd163d691c35", null ],
    [ "inv", "lab7__FrontUI_8py.html#afc71ed8d8924f4fcc41908619c0fa595", null ],
    [ "J", "lab7__FrontUI_8py.html#aa7d51eac9ede2c301dd6093c71b5eff1", null ],
    [ "J_list", "lab7__FrontUI_8py.html#a3f49b59c9d6d8c6e5fc554245b3bf432", null ],
    [ "line", "lab7__FrontUI_8py.html#a090937e41b39ad2857967bf0c522019a", null ],
    [ "n", "lab7__FrontUI_8py.html#a99b59982aa4ea8eb7956328516be59f1", null ],
    [ "omega_diff", "lab7__FrontUI_8py.html#a2e7e3903f943c10510b407ce5cf7a985", null ],
    [ "p", "lab7__FrontUI_8py.html#aec0d9ae0802cfa7f625e7b4974f9266a", null ],
    [ "phpos", "lab7__FrontUI_8py.html#a1e15f148b04cdd410f8e1d07cfc8793e", null ],
    [ "phspeed", "lab7__FrontUI_8py.html#a174b2fa75d43c076abf0a293fdff55d9", null ],
    [ "phtime", "lab7__FrontUI_8py.html#a2bb0e0109cbae923fbde8dabe91ca44a", null ],
    [ "position", "lab7__FrontUI_8py.html#a714a470def88e84db73950a34c8e3586", null ],
    [ "ref", "lab7__FrontUI_8py.html#a9fd0105af866699919350fe8e33a764e", null ],
    [ "ref_position", "lab7__FrontUI_8py.html#a9142490a96f67d4d9500f71dad36b457", null ],
    [ "ref_speed", "lab7__FrontUI_8py.html#a940a46cb487b11587bc9c6702d48e084", null ],
    [ "ref_time", "lab7__FrontUI_8py.html#a42a2b8b9f615c781fc60324d0ef8e12d", null ],
    [ "sent_cmd", "lab7__FrontUI_8py.html#a6c8ba4012b085ffaae314fcdc02a9359", null ],
    [ "ser", "lab7__FrontUI_8py.html#a0426f8c6f1fdcb360bc1091ac5e5068d", null ],
    [ "speed", "lab7__FrontUI_8py.html#abb5c99242328ca90ec7c9b9214e5af98", null ],
    [ "t", "lab7__FrontUI_8py.html#a7afa8b8e201783d52fed7feb57cdf45c", null ],
    [ "time", "lab7__FrontUI_8py.html#a7ec13961d8a4fe33715b2e2fb549ebc8", null ],
    [ "time_index", "lab7__FrontUI_8py.html#ad8c2c44c26d195476e74ec38fe848cc8", null ],
    [ "v", "lab7__FrontUI_8py.html#a5d825ecccee0f40d8c562156fcf56f1a", null ],
    [ "x", "lab7__FrontUI_8py.html#aa29d16ad2658a8f853cdb2f4fa8b38c9", null ]
];