var classlab6__controller_1_1ClosedLoop =
[
    [ "__init__", "classlab6__controller_1_1ClosedLoop.html#a195d95954309d911352f3a33ae015d41", null ],
    [ "get_Kp", "classlab6__controller_1_1ClosedLoop.html#a24266c366f2f0059df1d4c021ae67632", null ],
    [ "set_Kp", "classlab6__controller_1_1ClosedLoop.html#ae15b88d63ebf25f9314bd9e480cc5c7c", null ],
    [ "update", "classlab6__controller_1_1ClosedLoop.html#a834c34a4bd5a91482a66a0999c41d489", null ],
    [ "duty", "classlab6__controller_1_1ClosedLoop.html#abcbfaddfd878e5c48ec015bedcd30e5d", null ],
    [ "K_p", "classlab6__controller_1_1ClosedLoop.html#a3057290e1963b24914cbc0cf4e4fbea4", null ],
    [ "newDuty", "classlab6__controller_1_1ClosedLoop.html#a5a9e364ad2f29f1dff75c2bb2e7de131", null ],
    [ "omega_ref", "classlab6__controller_1_1ClosedLoop.html#ac822e70f7ea3f758ff8578ae207769aa", null ]
];