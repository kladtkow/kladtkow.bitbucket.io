var annotated_dup =
[
    [ "HW0x00_FSM_ME305", null, [
      [ "Button", "classHW0x00__FSM__ME305_1_1Button.html", "classHW0x00__FSM__ME305_1_1Button" ],
      [ "elevatorInstance", "classHW0x00__FSM__ME305_1_1elevatorInstance.html", "classHW0x00__FSM__ME305_1_1elevatorInstance" ],
      [ "ElevatorPosition", "classHW0x00__FSM__ME305_1_1ElevatorPosition.html", "classHW0x00__FSM__ME305_1_1ElevatorPosition" ],
      [ "MotorDriver", "classHW0x00__FSM__ME305_1_1MotorDriver.html", "classHW0x00__FSM__ME305_1_1MotorDriver" ],
      [ "TaskElevator", "classHW0x00__FSM__ME305_1_1TaskElevator.html", "classHW0x00__FSM__ME305_1_1TaskElevator" ]
    ] ],
    [ "lab2b_FSM", null, [
      [ "mcuLED", "classlab2b__FSM_1_1mcuLED.html", "classlab2b__FSM_1_1mcuLED" ],
      [ "taskLEDPattern", "classlab2b__FSM_1_1taskLEDPattern.html", "classlab2b__FSM_1_1taskLEDPattern" ],
      [ "taskSawDimmer", "classlab2b__FSM_1_1taskSawDimmer.html", "classlab2b__FSM_1_1taskSawDimmer" ],
      [ "VirtualLED", "classlab2b__FSM_1_1VirtualLED.html", "classlab2b__FSM_1_1VirtualLED" ]
    ] ],
    [ "lab3_encoder", null, [
      [ "EncoderDriver", "classlab3__encoder_1_1EncoderDriver.html", "classlab3__encoder_1_1EncoderDriver" ],
      [ "TaskEncoder", "classlab3__encoder_1_1TaskEncoder.html", "classlab3__encoder_1_1TaskEncoder" ]
    ] ],
    [ "lab3_interface", null, [
      [ "TaskUI", "classlab3__interface_1_1TaskUI.html", "classlab3__interface_1_1TaskUI" ]
    ] ],
    [ "lab4_DataGen", null, [
      [ "EncoderDriver", "classlab4__DataGen_1_1EncoderDriver.html", "classlab4__DataGen_1_1EncoderDriver" ],
      [ "TaskDataGen", "classlab4__DataGen_1_1TaskDataGen.html", "classlab4__DataGen_1_1TaskDataGen" ]
    ] ],
    [ "lab4_DataUI", null, [
      [ "TaskDataUI", "classlab4__DataUI_1_1TaskDataUI.html", "classlab4__DataUI_1_1TaskDataUI" ]
    ] ],
    [ "lab5_interface", null, [
      [ "TaskUI", "classlab5__interface_1_1TaskUI.html", "classlab5__interface_1_1TaskUI" ]
    ] ],
    [ "lab5_LED", null, [
      [ "LEDDriver", "classlab5__LED_1_1LEDDriver.html", "classlab5__LED_1_1LEDDriver" ],
      [ "TaskBlinker", "classlab5__LED_1_1TaskBlinker.html", "classlab5__LED_1_1TaskBlinker" ]
    ] ],
    [ "lab6_controller", null, [
      [ "ClosedLoop", "classlab6__controller_1_1ClosedLoop.html", "classlab6__controller_1_1ClosedLoop" ],
      [ "EncoderDriver", "classlab6__controller_1_1EncoderDriver.html", "classlab6__controller_1_1EncoderDriver" ],
      [ "MotorDriver", "classlab6__controller_1_1MotorDriver.html", "classlab6__controller_1_1MotorDriver" ],
      [ "TaskMotorController", "classlab6__controller_1_1TaskMotorController.html", "classlab6__controller_1_1TaskMotorController" ]
    ] ],
    [ "lab6_NucleoUI", null, [
      [ "TaskNucleoUI", "classlab6__NucleoUI_1_1TaskNucleoUI.html", "classlab6__NucleoUI_1_1TaskNucleoUI" ]
    ] ],
    [ "lab7_controller", null, [
      [ "ClosedLoop", "classlab7__controller_1_1ClosedLoop.html", "classlab7__controller_1_1ClosedLoop" ],
      [ "EncoderDriver", "classlab7__controller_1_1EncoderDriver.html", "classlab7__controller_1_1EncoderDriver" ],
      [ "MotorDriver", "classlab7__controller_1_1MotorDriver.html", "classlab7__controller_1_1MotorDriver" ],
      [ "TaskMotorController", "classlab7__controller_1_1TaskMotorController.html", "classlab7__controller_1_1TaskMotorController" ]
    ] ],
    [ "lab7_NucleoUI", null, [
      [ "TaskNucleoUI", "classlab7__NucleoUI_1_1TaskNucleoUI.html", "classlab7__NucleoUI_1_1TaskNucleoUI" ]
    ] ]
];