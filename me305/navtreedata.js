/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Project Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0x01: Fibonacci Number Generator", "lab01.html", [
      [ "Problem Statement", "lab01.html#sec_problem1", null ],
      [ "Code Documentation", "lab01.html#documentation1", null ],
      [ "Source Code", "lab01.html#sourcecode1", null ]
    ] ],
    [ "Lab 0x02b: Virtual and Physical LED Pattern", "lab02b.html", [
      [ "Problem Statement", "lab02b.html#sec_problem2", null ],
      [ "Code Documentation", "lab02b.html#documentation2", null ],
      [ "Source Code", "lab02b.html#sourcecode2", null ]
    ] ],
    [ "Lab 0x03: Incremental Encoders", "lab03.html", [
      [ "Problem Statement", "lab03.html#sec_problem3", null ],
      [ "Code Documentation", "lab03.html#documentation3", null ],
      [ "Source Code", "lab03.html#sourcecode3", null ],
      [ "State Transition Diagram for the Encoder", "lab03.html#state_diagram_lab3a", null ],
      [ "State Transition Diagram for the Interface", "lab03.html#state_diagram_lab3b", null ]
    ] ],
    [ "Lab 0x04: Extending Your Interface", "lab04.html", [
      [ "Problem Statement", "lab04.html#sec_problem4", null ],
      [ "Code Documentation", "lab04.html#documentation4", null ],
      [ "Source Code", "lab04.html#sourcecode4", null ],
      [ "Example Data for Lab 4", "lab04.html#example_data_lab4", null ]
    ] ],
    [ "Lab 0x05: Use Your Interface", "lab05.html", [
      [ "Problem Statement", "lab05.html#sec_problem5", null ],
      [ "Code Documentation (Nucleo-Side)", "lab05.html#documentation5", null ],
      [ "Source Code (Nucleo-Side)", "lab05.html#sourcecode5", null ],
      [ "Source Code (Smartphone-Side)", "lab05.html#sourcecode5b", null ],
      [ "State Transition Diagram for the LED", "lab05.html#state_diagram_lab5a", null ],
      [ "State Transition Diagram for the Interface", "lab05.html#state_diagram_lab5b", null ],
      [ "Task Diagram for the System", "lab05.html#task_diagram_lab5", null ]
    ] ],
    [ "Lab 0x06: DC Motors", "lab06.html", [
      [ "Problem Statement", "lab06.html#sec_problem6", null ],
      [ "Code Documentation", "lab06.html#documentation6", null ],
      [ "Source Code", "lab06.html#sourcecode6", null ],
      [ "Responses for Different Proportional Gains", "lab06.html#example_data_lab6", null ],
      [ "State Transition Diagram for the Motor Controller", "lab06.html#state_diagram_lab6a", null ],
      [ "State Transition Diagram for the Interface", "lab06.html#state_diagram_lab6b", null ]
    ] ],
    [ "Lab 0x07: Reference Tracking", "lab07.html", [
      [ "Problem Statement", "lab07.html#sec_problem7", null ],
      [ "Code Documentation", "lab07.html#documentation7", null ],
      [ "Source Code", "lab07.html#sourcecode7", null ],
      [ "Reference vs Experimental Velocity and Position Profiles", "lab07.html#example_data_lab7", null ]
    ] ],
    [ "HW 0x00: Elevator Simulation", "hw0x00.html", [
      [ "Problem Statement", "hw0x00.html#sec_problem_hw_0", null ],
      [ "Code Documentation", "hw0x00.html#documentation_hw_0", null ],
      [ "State Transition Diagram", "hw0x00.html#state_diagram_hw0x00", null ],
      [ "Source Code", "hw0x00.html#sourcecodehw0x00", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW0x00__FSM__ME305_8py.html",
"classlab6__controller_1_1EncoderDriver.html#ac404e3c6fea233655cde523b35f73f2d",
"lab5__main_8py.html#a697e58208a40e983603e5d15b3317b29"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';