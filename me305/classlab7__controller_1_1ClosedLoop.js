var classlab7__controller_1_1ClosedLoop =
[
    [ "__init__", "classlab7__controller_1_1ClosedLoop.html#a2820477d83328eaace8683482de867a0", null ],
    [ "get_Kp", "classlab7__controller_1_1ClosedLoop.html#a84645cd540367633f3ae337b5e0d03e4", null ],
    [ "set_Kp", "classlab7__controller_1_1ClosedLoop.html#a38d6e87ecd69074006f9ca1627b90048", null ],
    [ "set_Omega", "classlab7__controller_1_1ClosedLoop.html#a13812f72a0c53899382783f07aa0c562", null ],
    [ "update", "classlab7__controller_1_1ClosedLoop.html#acd698d1fa132495944089ed1e6b69d94", null ],
    [ "duty", "classlab7__controller_1_1ClosedLoop.html#ade02716ecf6cca19afefaa4df204b9ed", null ],
    [ "K_p", "classlab7__controller_1_1ClosedLoop.html#ad1585ad36848c52137cd8511bba652fa", null ],
    [ "newDuty", "classlab7__controller_1_1ClosedLoop.html#a4145f99bf15a8cd56b63c85cb0eb96b7", null ],
    [ "omega_ref", "classlab7__controller_1_1ClosedLoop.html#a49c78c5906382cdb98584715cfffca0a", null ]
];