var classlab6__controller_1_1TaskMotorController =
[
    [ "__init__", "classlab6__controller_1_1TaskMotorController.html#a208d4c656ddae8649b096d40f9a99c02", null ],
    [ "printDebug", "classlab6__controller_1_1TaskMotorController.html#a975379d7c130af493c96f71daa170122", null ],
    [ "run", "classlab6__controller_1_1TaskMotorController.html#a3068c6e147daef8bc3e383b77f2fcb43", null ],
    [ "transitionTo", "classlab6__controller_1_1TaskMotorController.html#a9bff78bb9c1ff140f6b25e2191926a24", null ],
    [ "ClosedLoop", "classlab6__controller_1_1TaskMotorController.html#a0cea2ba3cded453591312f2d1b4b80b5", null ],
    [ "correctedDuty", "classlab6__controller_1_1TaskMotorController.html#a50e51b67dd7626a3fbbe0080e26e67fd", null ],
    [ "curr_time", "classlab6__controller_1_1TaskMotorController.html#a3872c400caed35c8f9624d2427482622", null ],
    [ "dbg", "classlab6__controller_1_1TaskMotorController.html#a5c609a3902187aec1f552c94aad0af99", null ],
    [ "encoder", "classlab6__controller_1_1TaskMotorController.html#ac8daedc70b2d46f3648400f3fc7c6a69", null ],
    [ "interval", "classlab6__controller_1_1TaskMotorController.html#a34a754767182a26345380d9130518d07", null ],
    [ "motor", "classlab6__controller_1_1TaskMotorController.html#a1beb67e1559ee26d579a724da46da4fa", null ],
    [ "new_Kp", "classlab6__controller_1_1TaskMotorController.html#afda6847494741ce6bbb0e74d7002b0e8", null ],
    [ "next_sample", "classlab6__controller_1_1TaskMotorController.html#afe03aeed13f96a3c939fa368aeb26da5", null ],
    [ "next_time", "classlab6__controller_1_1TaskMotorController.html#ad182eb897957051b57515acd80dd5f50", null ],
    [ "runs", "classlab6__controller_1_1TaskMotorController.html#ac0293060484a7baf8c70bb95bcd93504", null ],
    [ "sample_interval", "classlab6__controller_1_1TaskMotorController.html#a4a30c9b277533ad9fdca4f0b54e79e16", null ],
    [ "start_time", "classlab6__controller_1_1TaskMotorController.html#ab9e068dade40076332f61e790aa1d225", null ],
    [ "startTime", "classlab6__controller_1_1TaskMotorController.html#aac07ab921dfcc0ddc34fbbe82856f7f0", null ],
    [ "state", "classlab6__controller_1_1TaskMotorController.html#a2c089d714bc729c70c35b0b5e004c7fa", null ],
    [ "taskNum", "classlab6__controller_1_1TaskMotorController.html#ac97fcbebf1c27108f8632bece7df0499", null ]
];