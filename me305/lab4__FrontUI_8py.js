var lab4__FrontUI_8py =
[
    [ "readSerial", "lab4__FrontUI_8py.html#ab147b3df74d3c5808fa7c5f4a6b377c1", null ],
    [ "sendChar", "lab4__FrontUI_8py.html#aa7c59dbd9132ee8e1c11a7464ae99cf4", null ],
    [ "angle", "lab4__FrontUI_8py.html#a9a0805bcea9000191fa55a8994f50a7d", null ],
    [ "ax", "lab4__FrontUI_8py.html#a626a0f91ffc98a34f59161e369d1b910", null ],
    [ "data", "lab4__FrontUI_8py.html#a03d3ab5fce52bc862ca7f0a0a3825325", null ],
    [ "dataStop", "lab4__FrontUI_8py.html#a0593007eb398b13081581f046c3778b1", null ],
    [ "endScript", "lab4__FrontUI_8py.html#abd595590f7fcbda922ac137c1d3466b6", null ],
    [ "fig", "lab4__FrontUI_8py.html#afc146c63ead6ec6ec74c2feea90ee01f", null ],
    [ "inv", "lab4__FrontUI_8py.html#af8388188a1d6271220015a01ea97e80b", null ],
    [ "n", "lab4__FrontUI_8py.html#a1d321e01b0954cd02635e346766fcbe8", null ],
    [ "p", "lab4__FrontUI_8py.html#ab8c336170b32fea93b3a8d37a9cab3bc", null ],
    [ "phpos", "lab4__FrontUI_8py.html#abb6a4b7d71d06ede637b403b5101e0a2", null ],
    [ "phtime", "lab4__FrontUI_8py.html#af8b72b814e69d95bcc8baa83b22c914a", null ],
    [ "ser", "lab4__FrontUI_8py.html#aaecad82d95f1bf399ef32ade6780f942", null ],
    [ "time", "lab4__FrontUI_8py.html#aae3d76d8082ab1ad0c2189942016dea9", null ]
];